*** Settings ***
Library  SeleniumLibrary
Documentation  This test suite, adds products from "Günün Fırsatları" and calculates the amount left for free shipping
Resource  ../Resources/CheckChartApp.robot
Resource  ../Resources/CommonWeb.robot
Test Setup  Begin Web Test
Test Teardown  End Web Test

# robot -d results tests/checkChart.robot

*** Variables ***
${BROWSER} =  chrome
${URL} =  https://www.hepsiburada.com/
${CHECKOUT_URL} =  https://www.hepsiburada.com/ayagina-gelsin/sepetim

*** Test Cases ***

Go to website and add products to chart
    [Documentation]  This case tests the free shipping eligibility
    [Tags]  smoke
    Given CheckChartApp.Go to Main Page
    And sleep  3s
    When CheckChartApp.Go to Checkout Page
    Then CheckChartApp.Validate Calculation



